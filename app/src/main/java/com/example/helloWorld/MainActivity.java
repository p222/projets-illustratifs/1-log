package com.example.helloWorld;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;


public class MainActivity extends AppCompatActivity {

    // information qui apparaitra dans le log (à adapter en fonction des besoins)
    public static final String TAG = "MainActivity:LOG";
    // ou
    //public static final String TAG = MainActivity.class.getSimpleName();
    // ou autre combinaison String pour pouvoir la retrouver facilement dans le log...


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Affichage d'un message débug dans le log
        Log.d(MainActivity.TAG, "message debug");

        // Affichage d'un message verbose dans le log
        Log.v(MainActivity.TAG, "message verbeux");

        // Affichage d'un message d'erreur dans le log
        Log.e(MainActivity.TAG, "un message d'erreur qui n'en est pas un....");

        //...
    }
}